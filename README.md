This is a project for evaluating clearing protocols on 3D spheroid stacks.

How to download
===
The best way to download the plugin (and keep it up to date at the same time) is to use the Fiji/ImageJ updater.

The update site of the plugin is:

* URL: https://sites.imagej.net/Dhirling/
* Name: Spheroid Quality Measurement

To add this update site to your Fiji installation, please follow [this tutorial](https://imagej.net/Following_an_update_site) on [imagej.net](https://imagej.net/).

How to start this plugin
===
Once you have downloaded the necessary files and dependecies to the appropriate directories, you will find the plugin by clicking the "plugins" button in ImageJ:

![Image of how to open the plugin.](img/howToOpen.png)

You might have more plugins compared to the image above and it is possible that you have to scroll to te very bottom to find this plugin.

When clicking on *Spheroid Quality Measurement*, you should see the following graphical interface: 

![Image of the base GUI.](img/gui.png)

Setting the parameters
===
## Metric
You can choose which quality metric you want to use in the first row. These metrics are the following:

* Intensity variance (3.)
* Gradient magnitude variance (3.)
* Laplacian variance (3.)
* Histogram thresholding (4.)
* Histogram entropy (4.)
* Frequency thresholding (2.)
* Kurtosis (5.).

For metrics where it is possible to restrict the calculation to certain pixels, you will have the option to check the *Calculate metric inside a circle* and *Do not threshold* checkboxes.

If you check *Calculate metric inside a circle*, you will be able to specify the exact radius of the circle mask that you want to use. The calculation of the mask and the circle centroid is described in the article. The recommended (default) radius is 200 pixels. For a reference, this circle has the following size on a 2048x2048 slice:

![Inside circle](img/insidecircle.png)

Note that the center of the circle is not the center of the image, but the center of mass of the spheroid on each slice. 

## Image channels

For multichannel images, you have the option to select which channel you want to process at *Channel to extract*. Otherwise, if your image consists of only 1 channel, you should just leave everything as is.

## Spheroid thresholding

If you check *Do not threshold*, the initial Otsu thresholding will not be applied to the images.

## Pixel cutoff

If you have some outlying, very bright intensities on your images, you have the option to ignore those values and normalize the image without taking the brightest pixels into consideration. You can do it by modifying the *Pixel cutoff* value. The value you set here will be the percentage of the brightest pixels in the **entire** 3D volume that will be ignored during the metric calculation. Be careful with this, as even a 1% cutoff is a very drastic change.

## Export

You can choose which images you want to compare to each other by clicking on the *Choose images to compare...* button. Note that you have to select all of the images you wish to compare in this selection period, because clicking on the choose button another time will overwrite previous selections.

There are some other important aspects to consider when choosing images that you wish to compare:

* The currently accepted file formats are *.tif* and *.tiff*.
* The images have to be 3 dimensional.
* It is advised for the images that are compared to be of the same representation (*e.g.* both of them are 8/16/32 bit), in other cases, the measurements are not guaranteed to work correctly.

By clicking *Choose export directory...* you will be able to specify the directory where you want to save the scores for the chosen images. These scores will be saved in a CSV file at the end of the metric calculation.

Running the calculation
===

Once everything is set, clicking the *Calculate metric* button will start running the evaluations. Next to this button, the *Status* bar will provide information about the progress. The evaluation might take some time depending on the number of images you have and the metric that you have chosen.

When the software has finished processing all of your images, you will get a plot with each curve corresponding to one of the provided images:

![Image of the plots](img/plot.png)

The horizontal axis represents the index of the z-slice, whereas on the vertical axis you can see the metric values calculated for certain slices.

You can also save the plots by clicking on *Data > Save data...*

How to interpret the results
===
At the end of the calculations, you will have your metric scores in the file that you chose at *Choose export directory...*. By default, this file will be named *results.csv*. If you are using Excel, you can load a csv file in table format by clicking on *Data > From Text/CSV*:

![csv](img/csv.png)

After performing this operation, the table should have the following structure:

|Name|Glob|Top|Mid|Bot|
|---|---|---|---|---|
|Spheroid1.tif|...|...|...|...|
|Spheroid2.tif|...|...|...|...|
|Spheroid3.tif, etc.|...|...|...|...|

The *Name* column contains all of the image names that have been compared.

For the detailed calculation of the scores, see the article, but the intuition should be the following (for *Glob*):

For each measurement, we take the global maximum value of all of the plots, and create a rectangle with the height of the global maximum minus the global minimum (typically 0) and the width of our current plot. Our global score for the measurement is the area under the plot divided by the area of the rectangle mentioned before. This value is finally scaled between 1 and 5, where 1 is the worst, and 5 is the best possible score to get.

The *Top*, *Mid*, and *Bot* scores are calculated the same way with the exception that these scores only take a certain part of the plots into consideration, and calculate the maximums, widths and areas based on the respective part of the plots.

![Score calculation](img/score.png)

References
===

1. R. Ferzli, L. J. Karam, A no-reference objective image sharpness metric based on just-noticeable blur and probability summation. Proceedings - International Conference on Image Processing, ICIP. 3, 445–448 (2006).
2. C. F. Batten, Autofocusing and Astigmatism Correction in The Scanning Electron Microscope. University of Cambridge, 1–89 (2000).
3. J. Pech-Pacheco, J.L., Cristobal, G., Chamorro-Martinez, J., Fernandez-Valdivia, Diatom autofocusing in brightfield microscopy: A comparative study. 15th International Conference on Pattern Recognition. 3, 314–317 (2000).
4. L. Firestone, K. Cook, K. Culp, N. Talsania, K. Preston, Comparison of autofocus methods for automated microscopy. Cytometry. 12, 195–206 (1991).
5. N. Zhang, A. Vladar, M. Postek, B. Larrabee, A kurtosis-based statistical measure for two-dimensional processes and its application to image sharpness. Proceedings Section of Physical and Engineering Sciences of American Statistical Society, 4730–4736 (2003).

How to cite us
===
A. Diosdi, D. Hirling, M. Kovacs, T. Toth, M. Harmati, K. Koos, K. Buzas, F. Piccinini, P. Horvath: A quantitative metric for the comparative evaluation of optical clearing protocols for 3D multicellular spheroids