package hu.brc.hpg.imagej;

import ij.ImageStack;
import ij.gui.Plot;
import ij.io.Opener;
import ij.plugin.ChannelSplitter;
import ij.plugin.Duplicator;
import ij.plugin.FFT;
import ij.plugin.Histogram;
import ij.plugin.filter.Convolver;
import ij.process.*;
import net.imagej.ops.Op;
import net.imglib2.img.Img;
import net.imglib2.img.ImagePlusAdapter;

import ij.ImagePlus;
import org.ojalgo.matrix.transformation.Rotation;

import javax.swing.*;
import java.awt.*;
import java.awt.image.ColorModel;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;




public class PluginController {
    private PluginModel model;
    private PluginUI ui;
    private String saveFileName;
    //private List<List<File>> fileBatches;
    public PluginController(PluginModel m, PluginUI u) {
        model = m;
        ui = u;
    }

    public PluginController() {

    }

    public void initView() {

    }

    public void initController() {
        Runnable calcThread = new Runnable() {
            public void run() {
                try {
                    calculateMetric();
                } catch (IOException v) {
                    System.out.println(v);
                }
            }
        };


        ui.getChooseFilesButton().addActionListener(e -> ui.showFileChooser());
        ui.getFileChooser().addActionListener(e -> {
            if (e.getActionCommand()=="ApproveSelection")
                ui.setChosenFilesList();
        });

        ui.getExportDirButton().addActionListener(e -> ui.showExportChooser());

        ui.getSaveFileChooser().addActionListener(e -> {
            if (e.getActionCommand()=="ApproveSelection") {
                saveFileName = ui.getSaveFile();
                ui.setSaveFile();
            }
        });

        ui.getCalculateMetricButton().addActionListener(e -> {
            ui.setMessage("starting metric calculation...");
            ui.getCalculateMetricButton().setEnabled(false);
            new Thread(calcThread).start();
        });

        ui.getCircleCheckbox().addActionListener(e -> {
            ui.setVisible(ui.getRadiusSpinner(), ui.getCircleCheckbox().isSelected());
            ui.setVisible(ui.getRadiusLabel(), ui.getCircleCheckbox().isSelected());
        });

        ui.getCircleCheckbox().addActionListener(e -> {
            ui.setVisible(ui.getRadiusSpinner(), ui.getCircleCheckbox().isSelected());
            ui.setVisible(ui.getRadiusLabel(), ui.getCircleCheckbox().isSelected());
        });

        ui.getMetricList().addActionListener(e -> {
            switch(ui.getMetricIndex()) {
                case 0: ui.enableAllOptions(); break; // intensity
                case 1: ui.enableAllOptions(); break; // deriv
                case 2: ui.enableAllOptions(); break; // laplacian
                case 3: ui.enableAllOptions(); break; // entropy
                case 4: ui.enableAllOptions(); break; // threshold
                case 5: ui.disableOption(ui.getThresholdCheckbox()); ui.disableOption(ui.getCircleCheckbox()); ui.disableOption(ui.getCutoffSpinner()); ui.setVisible(ui.getRadiusLabel(),false); ui.setVisible(ui.getRadiusSpinner(),false); break; // frequency
                case 6: ui.disableOption(ui.getThresholdCheckbox()); ui.disableOption(ui.getCircleCheckbox()); ui.disableOption(ui.getCutoffSpinner()); ui.setVisible(ui.getRadiusLabel(),false); ui.setVisible(ui.getRadiusSpinner(),false); break; // kurtosis
                default: ui.enableAllOptions(); break;
            }
        });

        ui.getAboutUsButton().addActionListener(e -> {
            ui.showAboutUs();
        });



    }

    private void calculateMetric() throws IOException {
        List<File> files;

        if (ui.getCutoff() < 0 || ui.getCutoff() > 100) {
            ui.errorMessage("Cutoff percentage can't be higher than 100 or lower than 0.");
            ui.setMessage("No image operations in progress.");
            ui.getCalculateMetricButton().setEnabled(true);
            return;
        }

        if (saveFileName == null) {
            ui.errorMessage("You haven't specified an export directory for the scores. Please provide one to continue.");
            ui.setMessage("No image operations in progress.");
            ui.getCalculateMetricButton().setEnabled(true);
            return;
        }

        try {
            files = ui.getChosenFilesList();
        } catch (IOException e) {
            ui.errorMessage("There was an error with one or more of the provided input files. Reason of error: "+e);
            ui.setMessage("No image operations in progress.");
            ui.getCalculateMetricButton().setEnabled(true);
            return;
        }
        String plotLegend = "";
        for (int i=0; i<files.size(); i++) {
            System.out.println(files.get(i).getCanonicalPath());
            plotLegend+=files.get(i).getName()+"\n";
        }

        Plot metricPlot = new Plot("Metric values", "slice", "metric");
        int cnt=0;
        double[] sums = new double[files.size()];
        double[] maximums = new double[files.size()];
        double[] widths = new double[files.size()];
        double[] metrics = new double[files.size()];
        String[] names = new String[files.size()];
        int[] top = new int[files.size()];
        int[] mid = new int[files.size()];
        int[] bot = new int[files.size()];
        double[] topsum = new double[files.size()];
        double[] midsum = new double[files.size()];
        double[] botsum = new double[files.size()];
        double maxtop = 0;
        double maxmid = 0;
        double maxbot = 0;
        double globMin = Double.MAX_VALUE;


        model.setMetricMaximum(0.0);
        model.setWidthMaximum(0);

        for (File file: files) {
            cnt++;
            String curr_msg = cnt+"/"+files.size()+": Processing "+file.getName();

            ui.setMessage(curr_msg+", reading image...");


            ImagePlus[] imps = ChannelSplitter.split(new Opener().openImage(file.getAbsolutePath()));
            if (ui.getChannelIndex() > imps.length -1) {
                ui.errorMessage("The channel to be extracted is higher than the total number of channels in this image. If your image has only 1 channel, please choose \"Channel 1\".");
                ui.setMessage("No image operations in progress.");
                ui.getCalculateMetricButton().setEnabled(true);
                return;
            }
            ImagePlus imp = imps[ui.getChannelIndex()];

            int numSlices = imp.getNSlices();
            if (numSlices < 2) {
                ui.popupMessage("The image currently under processing is only 2 dimensional. This image will be ignored during the calculation.");
                continue;
            }

            ui.setMessage(curr_msg+", converting image to grayscale...");
            ImagePlus imp_mask = new Duplicator().run(imp);
            new ImageConverter(imp).convertToGray32();
            new ImageConverter(imp_mask).convertToGray16();


            // set the variables for the metric
            model.setCurrentImage(imp);
            model.setCurrentImageMask(imp_mask);
            model.setStackMaximum(getMaximum(imp));
            model.setCurrentImageProcessor(imp.getProcessor());
            model.setHeight(model.getCurrentImageProcessor().getHeight());
            model.setWidth(model.getCurrentImageProcessor().getWidth());
            model.setMetric(getMetricFromIndex(ui.getMetricIndex()));
            model.setCircleMetric(ui.getCircleCheckbox().isSelected());
            try {
                if (ui.getCircleCheckbox().isSelected()) model.setCircleRadius(ui.getRadius());
            } catch (Exception e) {
                ui.errorMessage("The currently selected inner circle radius is too high for this image resolution. please choose a lower one.");
                ui.setMessage("No image operations in progress.");
                ui.getCalculateMetricButton().setEnabled(true);
                return;
            }
            model.setMetricValues(new ArrayList<Double>());

            ui.setMessage(curr_msg+", calculating cutoff (if there is one)...");
            if (ui.getCutoff()>0.0) {
                model.setAllowedMaxIntensity(getTopPercent(imp, ui.getCutoff()));
            } else {
                model.setAllowedMaxIntensity((float)model.getStackMaximum());
            }
            // metric calculation
            processImageStack(curr_msg);

            //imp.show();
            // imp_mask.show();
            //imp_mask.show();
            //imp.show();



            // metric plotting & score calculation
            sums[cnt-1] = 0;
            double[] yvals = new double[model.getMetricValues().size()];
            if (model.getMetricValues().size()>model.getWidthMaximum())
                model.setWidthMaximum(model.getMetricValues().size());
            for (int j = 0; j<yvals.length; j++) {
                yvals[j] = model.getMetricValues().get(j);
                if (Double.isNaN(yvals[j]))
                    yvals[j] = 0;
                if (yvals[j] < globMin) {
                    // for subtraction later
                    globMin = yvals[j];
                }
                sums[cnt-1]+=yvals[j];
                if (yvals[j]>model.getMetricMaximum())
                    model.setMetricMaximum(yvals[j]);
            }

            metricPlot.setColor(getColorFromIndex(cnt));
            metricPlot.add("line", yvals.clone());

            ui.setProgress((int)(cnt/files.size()*100));

            widths[cnt-1] = model.getMetricValues().size();
            maximums[cnt-1] = model.getMetricMaximum();

            //indices of certain regions on the metric plot
            top[cnt-1] = 0;
            mid[cnt-1] = (int) /*Math.round*/myRound(widths[cnt-1]/3);
            bot[cnt-1] = (int) /*Math.round*/myRound(2*widths[cnt-1]/3);

            // sum these parts separately
            topsum[cnt-1] = 0;
            midsum[cnt-1] = 0;
            botsum[cnt-1] = 0;

            for (int j = 0; j < yvals.length; j++) {
                if (j < mid[cnt-1]) {
                    topsum[cnt-1] += yvals[j];
                    if (maxtop<yvals[j])
                        maxtop = yvals[j];
                }
                if (j>=mid[cnt-1] && j<bot[cnt-1]) {
                    midsum[cnt-1] += yvals[j];
                    if (maxmid<yvals[j])
                        maxmid = yvals[j];
                }
                if (j>=bot[cnt-1]) {
                    botsum[cnt-1] += yvals[j];
                    if (maxbot<yvals[j])
                        maxbot = yvals[j];
                }
            }

            names[cnt-1] = file.getName();

        }

        System.out.println("GlobMin: "+globMin);
        System.out.println("----------- Indices for files: ----------");
        for (int i=0; i<metrics.length; i++) {
            System.out.println(names[i]+":");
            System.out.println("Middle index: " + mid[i]);
            System.out.println("Bottom index: " + bot[i]);
        }
        // we want the global minimum to be 0
        // System.out.println("!!!!!!!!!!SETTING GLOBMIN TO 0!!!!!!!!!!");
        // globMin = 0;
        maxtop -= globMin;
        maxbot -= globMin;
        maxmid -= globMin;
        for (int i=0; i<metrics.length; i++) {
            maximums[i] -= globMin;
            sums[i] -= globMin*widths[i];
            topsum[i] -= globMin*mid[i];
            midsum[i] -= globMin*(bot[i]-mid[i]);
            botsum[i] -= globMin*(widths[i]-bot[i]);
        }
        double globMax = 0;
        for (int i=0; i<metrics.length; i++) {
            if (maximums[i]>globMax)
                globMax = maximums[i];
        }


        // write out the scores and normalize between 1 and 5
        try {
            FileWriter myFile = new FileWriter(saveFileName);
            myFile.write("name;Glob;Top;Mid;Bot\n");
            for (int i=0; i<metrics.length; i++) {
                myFile.write(names[i]+";"+(4*(sums[i]/(globMax*widths[i]))+1)+";"+(4*(topsum[i]/(maxtop*mid[i]))+1)+";"+(4*(midsum[i]/(maxmid*(bot[i]-mid[i])))+1)+";"+(4*(botsum[i]/(maxbot*(widths[i]-bot[i])))+1)+"\n");
            }
            myFile.close();
        } catch (IOException e) {
            ui.popupMessage("File Name: "+saveFileName+", Error with writing to file. The results were not saved. reason for error: "+e);

        }


        metricPlot.addLegend(plotLegend);

        metricPlot.show();

        ui.setMessage("No image operations in progress.");

        ui.getCalculateMetricButton().setEnabled(true);

    }

    // search for the maximum intensity value in a 3D ImagePlus stack
    private double getMaximum(ImagePlus imp) {
        ImageStatistics stats;
        double currMax = 0;
        for (int i=0; i<imp.getStackSize(); i++) {
            imp.setSlice(i);
            stats = imp.getStatistics();
            if (stats.max>currMax) {
                currMax = stats.max;
            }
        }
        System.out.println("Max: "+currMax);
        return currMax;
    }

    private float getTopPercent(ImagePlus imp, double percent) {
        System.out.println("start");
        float[] pixels = new float[model.getHeight()*model.getWidth()*imp.getStackSize()];
        int count=0;
        for (int i=0; i<imp.getStackSize(); i++) {
            imp.setSlice(i);
            float[] curr = (float[]) imp.getProcessor().getPixels();
            for (int x=0; x<curr.length; x++) {
                pixels[count++] = curr[x];
            }

        }
        Arrays.sort(pixels);
        int size = model.getHeight()*model.getWidth()*imp.getStackSize();
        System.out.println("pixels highest: (index "+ (size-1) +"): "+pixels[size-1]);
        System.out.println("pixels top percent:(index "+ (size-1-size*(percent/100.0)) +"): "+pixels[(int)Math.ceil(size-1-size*(percent/100.0))]);

        System.out.println("Number of Pixels considered: "+ ((size-1)-(int)Math.ceil(size-1-size*(percent/100.0))));
        float retval = pixels[(int)Math.ceil(size-size*(percent/100)-1)];
        pixels = null;
        return retval;
    }

    // get histogram entropy from nonzero elements of an image
    private double getHistogramEntropy(ImagePlus imp, int slice) {

        ImageProcessor ip = model.getCurrentImage().getStack().getProcessor(slice);
        ImageProcessor ipMask = model.getCurrentImageMask().getStack().getProcessor(slice);

        // Apply the Otsu mask on the mask image and save threshold value
        ipMask.setAutoThreshold("Otsu");
        model.setThresholdValue(ipMask.getAutoThreshold());
        ipMask.autoThreshold();

        maskPixelsHist( (float[]) ip.getPixels(), (short[]) ipMask.getPixels() );

        float[] pixels = (float[])ip.getPixels();
        short[] mpixels = (short[])ipMask.getPixels();
        for (int y=0; y < model.getHeight(); y++) {
            for (int x = 0; x < model.getWidth(); x++) {
                if (pixels[x + y * model.getWidth()]>model.getAllowedMaxIntensity() || mpixels[x + y * model.getWidth()] == 0 || mpixels[x + y * model.getWidth()] == -1)
                    pixels[x + y * model.getWidth()] = -1;
            }
        }

        ShortStatistics stats = new ShortStatistics(ip);

        imp.setSlice(slice);

        ip.setHistogramSize((int)model.getStackMaximum()+1);
        ip.setHistogramRange(0, model.getStackMaximum()+1);
        ip.setSliceNumber(slice);
        int[] h = ip.getHistogram();
        //double[] h_double = new double[h.length];
        double tmp;
        long sum = 0;
        double entropy = 0;
        for (int j=0; j<h.length; j++) {
            sum += h[j];
        }
        for (int j=0; j<h.length; j++) {
            tmp = (double) h[j]/sum;
            if (tmp!=0)
                entropy += tmp*(Math.log(tmp)/Math.log(2));
        }

        entropy*=-1;


        return entropy;
    }

    private long getHistogramThresholdValue(ImagePlus imp, int slice) {
        ImageProcessor ip = model.getCurrentImage().getStack().getProcessor(slice);
        ImageProcessor ipMask = model.getCurrentImageMask().getStack().getProcessor(slice);

        // Apply the Otsu mask on the mask image and save threshold value
        if (ui.getThresholdCheckbox().isSelected()) {
            ipMask.setValue(255);
        } else {
            ipMask.setAutoThreshold("Otsu");
            model.setThresholdValue(ipMask.getAutoThreshold());
            ipMask.autoThreshold();
        }

        maskPixelsHist((float[]) ip.getPixels(), (short[]) ipMask.getPixels());

        /*
        ImagePlus tmp = new ImagePlus();
        tmp.setProcessor(ipMask);
        tmp.show();*/


        float[] pixels = (float[])ip.getPixels();
        short[] mpixels = (short[])ipMask.getPixels();
        for (int y=0; y < model.getHeight(); y++) {
            for (int x = 0; x < model.getWidth(); x++) {
                if (pixels[x + y * model.getWidth()]>model.getAllowedMaxIntensity() || mpixels[x + y * model.getWidth()] == (short)-1 || mpixels[x + y * model.getWidth()] == 0)
                    pixels[x + y * model.getWidth()] = -1;
            }
        }

        ShortStatistics stats = new ShortStatistics(ip);
        //ip.setHistogramSize((int)model.getStackMaximum()+1);
        imp.setSlice(slice);
        ip.setSliceNumber(slice);
        //stats = imp.getStatistics();
        ip.setHistogramSize((int)model.getStackMaximum()+1);
        ip.setHistogramRange(0, model.getStackMaximum()+1);
        int[] h = ip.getHistogram();
        /*if (slice==2) {
            double[] htmp = new double[h.length];
            for (int i = 0; i < h.length; i++) {
                htmp[i] = h[i];
            }
            Plot test = new Plot("Hist", "Bin", "val");
            test.addHistogram(htmp);
            test.draw();
            test.show();
        }*/


        /*ImageProcessor tmp = model.getCurrentImage().getStack().getProcessor(1);
        float[] pixelstmp = (float[])tmp.getPixels();
        double t = Math.ceil(getMean(pixelstmp));*/
        double t = Math.ceil(getMean(pixels));
        System.out.println("Going from value "+t);
        long sum = 0;

        for (int j = (int) t; j<h.length; j++) {
            sum += j*h[j];
            if (h[j]<0) {
                System.out.println(j+": "+h[j]);
            }

        }
        System.out.println(sum);
        return sum;

    }

    // calculate mean excluding masked out values
    private double getMean(float[] pixels) {
        double sum=0;
        int count=0;
        for (int y=0; y < model.getHeight(); y++) {
            for (int x = 0; x < model.getWidth(); x++) {
                if (!(pixels[x + y * model.getWidth()]>model.getAllowedMaxIntensity() || pixels[x + y * model.getWidth()] == -1)) {
                    sum += pixels[x + y * model.getWidth()];
                    count++;
                }
            }
        }
        return sum/count;
    }

    // get whole 3D stack and process image slice by slice
    private void processImageStack(String curr_msg) {
        for (int i = 1; i <= model.getCurrentImage().getStackSize(); i++) {
            switch (model.getMetric()) {
                case ENTROPY: model.addMetricValue(getHistogramEntropy(model.getCurrentImage(), i)); break;
                case THRESHOLD: model.addMetricValue(getHistogramThresholdValue(model.getCurrentImage(), i)); break;
                case FREQUENCY: processSliceFrequency(i); break;
                case KURTOSIS: model.addMetricValue(kurtosis(model.getCurrentImage(), i)); break;
                default: processSlice(i); break;
            }
            ui.setMessage(curr_msg+ ", calculating metric on slice: "+i+"/"+model.getCurrentImage().getStackSize()+"...");
        }
    }

    public double myRound(double val) {
        if (val < 0) {
            return Math.ceil(val);
        }
        return Math.floor(val);
    }

    private void processSliceFrequency(int slice) {
        model.getCurrentImage().setSlice(slice);
        ImageProcessor ip = model.getCurrentImage().getProcessor();

        ImagePlus curr_fft = FFT.forward(model.getCurrentImage());
        model.setHeight(curr_fft.getHeight());
        model.setWidth(curr_fft.getWidth());
        ip = curr_fft.getProcessor();
        // model.setCircleRadius(50);
        maskInnerDisk( (byte[]) ip.getPixels() );
        /*if (slice>=25 && slice <= 30)
            curr_fft.show();*/
        model.addMetricValue( (double) /*(1.0/(4*200*200))**/sumPixels((byte[]) ip.getPixels()) );
    }

    private void maskInnerDisk( byte[] pixels ) {
        for (int y=0; y < model.getHeight(); y++) {
            for (int x=0; x < model.getWidth(); x++) {
                // (x-u)^2 + (y-v)^2 > r^2
                if (Math.pow(x-(model.getWidth()/2), 2)+Math.pow(y-(model.getHeight()/2), 2)<200*200)
                    pixels[x + y * model.getWidth()] = 0;

            }
        }
    }

    private double sumPixels(byte[] pixels ) {
        double sum = 0;
        for (int i=0; i<pixels.length; i++) {
            //sum.add(new BigDecimal(pixels[i]));
            if (pixels[i] < 0) {
                sum += (float)(127+(128+pixels[i]));
                /*System.out.println("before: "+pixels[i]);
                pixels[i] += 127;
                pixels[i] += 127;
                pixels[i] += 1;
                System.out.println("after: "+pixels[i]);*/

            } else {
                sum += pixels[i];
            }
        }
        return sum;
    }

    private void processSlice(int slice) {
        ImageProcessor ip = model.getCurrentImage().getStack().getProcessor(slice);
        ImageProcessor ipMask = model.getCurrentImageMask().getStack().getProcessor(slice);

        // Apply the Otsu mask on the mask image and save threshold value
        ipMask.setAutoThreshold("Otsu");
        model.setThresholdValue(ipMask.getAutoThreshold());
        ipMask.autoThreshold();

        /*if (slice==2) {
            ImagePlus tmp = new ImagePlus();
            tmp.setProcessor(ipMask);
            tmp.show();
        }*/

        // Apply another mask on top of Otsu if needed (e.g. circle mask)
        maskPixels( (float[]) ip.getPixels(), (short[]) ipMask.getPixels() );


        normalizePixels( (float[]) ip.getPixels());

        // change the image, but only if the metric requires it: TODO
        modifyImageForMetric(ip);

        // mask out pixels in the image with the otsu thresholded mask
        // maskPixels( (float[]) ip.getPixels(), (short[]) ipMask.getPixels() );

        // now we can get the variance inside the relevant part
        model.addMetricValue(getVariance( (float[]) ip.getPixels(), (short[]) ipMask.getPixels() ));

    }

    private void normalizePixels(float[] pixels) {
        for (int y=0; y < model.getHeight(); y++) {
            for (int x=0; x < model.getWidth(); x++) {
                /*if (pixels[x + y * model.getWidth()] > model.getAllowedMaxIntensity())
                    pixels[x + y * model.getWidth()] = model.getAllowedMaxIntensity();*/
                // process each pixel of the line
                // example: add 'number' to each pixel
                pixels[x + y * model.getWidth()] /= (float)model.getAllowedMaxIntensity();
            }
        }
    }

    private void modifyImageForMetric(ImageProcessor ip) {
        switch (model.getMetric()) {
            case INTENSITY: break;
            case DERIVATIVE: ip.findEdges(); break;
            case LAPLACIAN: createLaplacian(ip); break;
        }
    }

    private void createLaplacian(ImageProcessor ip) {
        Convolver c = new Convolver();
        float[] kernel = {0,-1,0,-1,4,-1,0,-1,0};
        c.convolve(ip,kernel,3,3);
    }

    // mask out pixels in the image with the otsu thresholded mask
    private void maskPixels(float[] imagePixels, short[] maskPixels) {
        long start, finish;

        // if calculating inside a circle, set the mask with the help of the circle equation
        if (model.isCircleMetric()) {
            start = System.currentTimeMillis();
            double m00 = getM(imagePixels, "00");
            finish = System.currentTimeMillis();
            //System.out.println("getm 1: "+ (finish-start));

            start = System.currentTimeMillis();
            long center_x = Math.round(getM(imagePixels, "10")/m00);
            finish = System.currentTimeMillis();
            //System.out.println("getm 2: "+ (finish-start));

            start = System.currentTimeMillis();
            long center_y = Math.round(getM(imagePixels, "01")/m00);
            finish = System.currentTimeMillis();
            //System.out.println("getm 3: "+ (finish-start));

            start = System.currentTimeMillis();
            for (int y=0; y < model.getHeight(); y++) {
                for (int x=0; x < model.getWidth(); x++) {
                    // (x-u)^2 + (y-v)^2 > r^2
                   if (Math.pow(x-center_x, 2)+Math.pow(y-center_y, 2)>Math.pow(model.getCircleRadius(),2))
                       maskPixels[x + y * model.getWidth()] = 0;

                }
            }
            finish = System.currentTimeMillis();
            System.out.println("masking done: "+ (finish-start));
        }

        /*for (int y=0; y < model.getHeight(); y++) {
            for (int x=0; x < model.getWidth(); x++) {

            }
        }*/



        for (int y=0; y < model.getHeight(); y++) {
            for (int x=0; x < model.getWidth(); x++) {
                if (imagePixels[x + y * model.getWidth()] > model.getAllowedMaxIntensity())
                    maskPixels[x + y * model.getWidth()] = 0;
            }
        }
    }

    private void maskPixelsHist(float[] imagePixels, short[] maskPixels) {
        long start, finish;

        // if calculating inside a circle, set the mask with the help of the circle equation
        if (model.isCircleMetric()) {
            start = System.currentTimeMillis();
            double m00 = getM(imagePixels, "00");
            finish = System.currentTimeMillis();
            //System.out.println("getm 1: "+ (finish-start));

            start = System.currentTimeMillis();
            long center_x = Math.round(getM(imagePixels, "10")/m00);
            finish = System.currentTimeMillis();
            //System.out.println("getm 2: "+ (finish-start));

            start = System.currentTimeMillis();
            long center_y = Math.round(getM(imagePixels, "01")/m00);
            finish = System.currentTimeMillis();
            //System.out.println("getm 3: "+ (finish-start));

            start = System.currentTimeMillis();
            for (int y=0; y < model.getHeight(); y++) {
                for (int x=0; x < model.getWidth(); x++) {
                    // (x-u)^2 + (y-v)^2 > r^2
                    if (Math.pow(x-center_x, 2)+Math.pow(y-center_y, 2)>Math.pow(model.getCircleRadius(),2))
                        maskPixels[x + y * model.getWidth()] = -1;

                }
            }
            finish = System.currentTimeMillis();
            System.out.println("masking done: "+ (finish-start));
        }

        /*for (int y=0; y < model.getHeight(); y++) {
            for (int x=0; x < model.getWidth(); x++) {

            }
        }*/



        for (int y=0; y < model.getHeight(); y++) {
            for (int x=0; x < model.getWidth(); x++) {
                if (imagePixels[x + y * model.getWidth()] > model.getAllowedMaxIntensity())
                    maskPixels[x + y * model.getWidth()] = -1;
            }
        }
    }

    private double getM(float[] pixels, String pq) {
        double m = 0;

        switch (pq) {
            case "00":
                for (int y = 0; y < model.getHeight(); y++) {
                    for (int x = 0; x < model.getWidth(); x++) {
                        m += pixels[x + y * model.getWidth()];
                    }
                }
                break;
            case "10":
                for (int y = 0; y < model.getHeight(); y++) {
                    for (int x = 0; x < model.getWidth(); x++) {
                        m += x*pixels[x + y * model.getWidth()];
                    }
                }
                break;

            case "01":
                for (int y = 0; y < model.getHeight(); y++) {
                    for (int x = 0; x < model.getWidth(); x++) {
                        m += y*pixels[x + y * model.getWidth()];
                    }
                }
                break;
        }
        return m;
    }

    private double getM(byte[] pixels, String pq) {
        double m = 0;

        switch (pq) {
            case "00":
                for (int y = 0; y < model.getHeight(); y++) {
                    for (int x = 0; x < model.getWidth(); x++) {
                        m += pixels[x + y * model.getWidth()];
                    }
                }
                break;
            case "10":
                for (int y = 0; y < model.getHeight(); y++) {
                    for (int x = 0; x < model.getWidth(); x++) {
                        m += x*pixels[x + y * model.getWidth()];
                    }
                }
                break;

            case "01":
                for (int y = 0; y < model.getHeight(); y++) {
                    for (int x = 0; x < model.getWidth(); x++) {
                        m += y*pixels[x + y * model.getWidth()];
                    }
                }
                break;
        }
        return m;
    }

    private double getM(byte[] pixels, int p, int q) {
        double m10 = getM(pixels, "10");
        double m01 = getM(pixels, "01");
        double m00 = getM(pixels, "00");
        double cx = m10/m00;
        double cy = m01/m00;
        double m = 0;

        for (int y = 0; y < model.getHeight(); y++) {
            for (int x = 0; x < model.getWidth(); x++) {
                double term = pixels[x + y * model.getWidth()];
                for (int i=0; i<p; i++) {
                    term *= (x-cx);
                }
                for (int j=0; j<q; j++) {
                    term *= (y-cy);
                }
                m += term;
                //m += Math.pow(x-cx, p)*Math.pow(y-cy, q)*pixels[x + y * model.getWidth()];
            }
        }
        //System.out.println("getM for "+p+","+q+": "+m);
        return m;
    }

    private double getVariance(float[] imagePixels, short[] maskPixels) {
        //calculate the mean
        double sum = 0;
        int nsize = 0;
        double mean;
        double sqDiff = 0;
        if (ui.getThresholdCheckbox().isSelected()) {
            for (int y = 0; y < model.getHeight(); y++) {
                for (int x = 0; x < model.getWidth(); x++) {
                    // TODO If circle checkbox is selected, we should take it into consideration here
                        sum += imagePixels[x + y * model.getWidth()];
                        nsize++;

                }
            }
        }
        else {
            for (int y = 0; y < model.getHeight(); y++) {
                for (int x = 0; x < model.getWidth(); x++) {
                    if (maskPixels[x + y * model.getWidth()] != 0) {
                        sum += imagePixels[x + y * model.getWidth()];
                        nsize++;
                    }
                }
            }
        }
        mean = sum/nsize;

        //System.out.println("mean: "+mean);


        //calculate squared difference
        if (ui.getThresholdCheckbox().isSelected()) {
            for (int y = 0; y < model.getHeight(); y++) {
                for (int x = 0; x < model.getWidth(); x++) {
                    // if (imagePixels[x + y * model.getWidth()] != 0) { WHY?
                        sqDiff += (imagePixels[x + y * model.getWidth()] - mean) * (imagePixels[x + y * model.getWidth()] - mean);
                    //}
                }
            }
        }
        else {
            for (int y = 0; y < model.getHeight(); y++) {
                for (int x = 0; x < model.getWidth(); x++) {
                    if (maskPixels[x + y * model.getWidth()] != 0) {
                        sqDiff += (imagePixels[x + y * model.getWidth()] - mean) * (imagePixels[x + y * model.getWidth()] - mean);
                    }
                }
            }
        }

        //the variance is:
        //System.out.println("var: "+sqDiff/nsize);
        return sqDiff/nsize;
    }

    private float gamma(int k, int l, float[] h, int width, int height, float m_w, float m_v, float sigma_w, float sigma_v) {
        float retval = 0;
        for (int i=0; i<width; i++) {
            for (int j=0; j<height; j++) {
                retval += ( h[i+j*width] * Math.pow(i+1 - m_w, k) * Math.pow(j+1 - m_v, l) );
            }
        }
        retval /= ( Math.pow(sigma_w, k) * Math.pow(sigma_v, l) );
        return retval;
    }

    // taken from the FHT class: changes labeled in the code
    void FHTps(int row, int maxN, float[] fht, float[] ps) {
        int base = row * maxN;

        for(int c = 0; c < maxN; ++c) {
            int l = (maxN - row) % maxN * maxN + (maxN - c) % maxN;
            ps[base + c] = (fht[base + c]*fht[base + c] + fht[l]*fht[l]) / 2.0F;
            // there were cases, where ps[0] could be infinite (overflow)
            if (Float.isInfinite(ps[base+c])) {
                ps[base+c] = Float.MAX_VALUE;
            }
        }

    }

    // taken from the FHT class: no changes in this function
    private ImageProcessor getPowerSpectrum(FHT myFht, int maxN) {
        float min = 3.4028235E38F;
        float max = 1.4E-45F;
        float[] fps = new float[maxN * maxN];
        byte[] ps = new byte[maxN * maxN];
        float[] fht = (float[])((float[])myFht.getPixels());

        int base;
        float r;
        int row;
        int col;
        for(row = 0; row < maxN; ++row) {
            this.FHTps(row, maxN, fht, fps);
            base = row * maxN;

            for(col = 0; col < maxN; ++col) {
                r = fps[base + col];
                if (r < min) {
                    min = r;
                }

                if (r > max) {
                    max = r;
                }
            }
        }

        max = (float)Math.log((double)max);
        min = (float)Math.log((double)min);
        if (Float.isNaN(min) || max - min > 50.0F) {
            min = max - 50.0F;
        }

        float scale = (float)(253.999D / (double)(max - min));

        for(row = 0; row < maxN; ++row) {
            base = row * maxN;

            for(col = 0; col < maxN; ++col) {
                r = fps[base + col];
                r = ((float)Math.log((double)r) - min) * scale;
                if (Float.isNaN(r) || r < 0.0F) {
                    r = 0.0F;
                }

                ps[base + col] = (byte)((int)(r + 1.0F));
            }
        }

        ImageProcessor ip = new ByteProcessor(maxN, maxN, ps, (ColorModel)null);
        myFht.swapQuadrants(ip);
        return ip;
    }

    private float kurtosis(ImagePlus imp, int slice) {
        ImagePlus tmp = new ImagePlus();
        ImageProcessor proc = imp.getImageStack().getProcessor(slice);
        FHT myFht = new FHT(proc);
        myFht.transform();

        FHT corrImg = myFht.conjugateMultiply(myFht);

        tmp.setProcessor(this.getPowerSpectrum(corrImg,corrImg.getWidth()));

        // tmp.show();

        int[] dims = tmp.getDimensions();


        byte[] bytePixels = (byte[])tmp.getProcessor().getPixels();
        float[] floatPixels = new float[bytePixels.length];
        // byte conversion to handle overflow: -127 == 0
        for (int s=0; s<bytePixels.length; s++) {
            if (bytePixels[s] < 0) {
                floatPixels[s] = (float) 127 + (128+ bytePixels[s]);
            } else {
                floatPixels[s] = (float) bytePixels[s];
            }
        }
        //System.out.println("probs: "+probs);
        FloatProcessor fp = new FloatProcessor(dims[0], dims[1], floatPixels);
        //float[] curr = floatPixels;
        float[] curr = (float[]) fp.getPixels();
        float sum = 0;
        // first, normalize the img so the sum becomes 1.
        for (int s=0; s<curr.length; s++) {
            sum += curr[s];
        }
        for (int s=0; s<curr.length; s++) {
            curr[s] /= sum;
        }

        //System.out.println("dims: ");
        /*for (int z=0; z<2;z++) {
            System.out.println(dims[z]);
        }*/

        tmp.setProcessor(fp);
        //tmp.show();

        // calculate the marginal means and variances
        float m_w = 0;
        for (int i=0; i<dims[0]; i++) {
            float term = 0;
            for (int j=0; j<dims[1]; j++) {
                term += curr[i+j*dims[0]];
            }
            term *= (i+1);
            m_w += term;
        }

        float m_v = 0;
        for (int j=0; j<dims[1]; j++) {
            float term = 0;
            for (int i=0; i<dims[0]; i++) {
                term += curr[i+j*dims[0]];
            }
            term *= (j+1);
            m_v += term;
        }

        // calculate sigmas
        float sigma_w = 0;
        for (int i=0; i<dims[0]; i++) {
            float term = 0;
            for (int j=0; j<dims[1]; j++) {
                term += curr[i+j*dims[0]];
            }
            term *= ((i+1)-m_w)*((i+1)-m_w);
            sigma_w += term;
        }
        sigma_w = (float) Math.sqrt(sigma_w);

        float sigma_v = 0;
        for (int j=0; j<dims[1]; j++) {
            float term = 0;
            for (int i=0; i<dims[0]; i++) {
                term += curr[i+j*dims[0]];
            }
            term *= ((j+1)-m_v)*((j+1)-m_v);
            sigma_v += term;
        }
        sigma_v = (float) Math.sqrt(sigma_v);

        // calculate covariance
        float cov_wv = 0;
        for (int i=0; i<dims[0]; i++) {
            for (int j=0; j<dims[1]; j++) {
                cov_wv += ( curr[i+j*dims[0]] * (i+1-m_w) * (j+1-m_v) );
            }
        }
        /*System.out.println("cov_wv = "+cov_wv);
        System.out.println("sigma_w = "+sigma_w);
        System.out.println("sigma_v = "+sigma_v);*/

        //calculate correlation
        float rho = (float) (cov_wv/(sigma_w*sigma_v));
        //System.out.println("rho = "+rho);

        float g22 = gamma(2,2, curr, dims[0], dims[1], m_w, m_v, sigma_w, sigma_v);

        // return with kurtosis, as defined in (3) in the article
        float nominator = gamma(4,0, curr, dims[0], dims[1], m_w, m_v, sigma_w, sigma_v);
        nominator += gamma(0,4, curr, dims[0], dims[1], m_w, m_v, sigma_w, sigma_v);
        nominator += 2*g22;
        nominator += + 4*rho*(rho*g22-gamma(1,3, curr, dims[0], dims[1], m_w, m_v, sigma_w, sigma_v)-gamma(3,1, curr, dims[0], dims[1], m_w, m_v, sigma_w, sigma_v));
        float denominator = (1-rho*rho)*(1-rho*rho);
        return nominator/denominator;
    }

    private Color getColorFromIndex(int idx) {
        switch (idx) {
            case 0: return Color.blue;
            case 1: return Color.red;
            case 2: return Color.green;
            case 3: return Color.cyan;
            case 4: return Color.GRAY;
            case 5: return Color.orange;
            case 6: return Color.magenta;
            case 7: return Color.darkGray;
            case 8: return Color.pink;
            default: return Color.black;
        }
    }

    private PluginModel.Metric getMetricFromIndex(int idx) {
        switch (idx) {
            case 0: return PluginModel.Metric.INTENSITY;
            case 1: return PluginModel.Metric.DERIVATIVE;
            case 2: return PluginModel.Metric.LAPLACIAN;
            case 3: return PluginModel.Metric.ENTROPY;
            case 4: return PluginModel.Metric.THRESHOLD;
            case 5: return PluginModel.Metric.FREQUENCY;
            case 6: return PluginModel.Metric.KURTOSIS;
            default: return PluginModel.Metric.INTENSITY;
        }
    }




}
