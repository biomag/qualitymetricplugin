package hu.brc.hpg.imagej;

import ij.ImagePlus;
import ij.process.ImageProcessor;

import java.io.IOException;
import java.util.List;



public class PluginModel {

    private Metric metric;
    private ImagePlus currentImage;
    private ImagePlus currentImageMask;
    private ImageProcessor currentImageProcessor;
    private int height;
    private int width;
    private List<Double> metricValues;
    private double stackMaximum;
    private int thresholdValue;
    private boolean circleMetric;
    private int circleRadius;
    private double metricMaximum;
    private int widthMaximum;
    private float allowedMaxIntensity;

    enum Metric {
        INTENSITY,
        DERIVATIVE,
        LAPLACIAN,
        ENTROPY,
        THRESHOLD,
        FREQUENCY,
        KURTOSIS
    }

    public PluginModel() {
        this.metric = Metric.INTENSITY;
        //this.circleRadius = 200;
    }

    public Metric getMetric() {
        return metric;
    }

    public void setMetric(Metric metric) {
        this.metric = metric;
    }

    public ImagePlus getCurrentImage() {
        return currentImage;
    }

    public void setCurrentImage(ImagePlus image) {
        this.currentImage = image;
    }

    public ImageProcessor getCurrentImageProcessor() { return currentImageProcessor; }

    public void setCurrentImageProcessor(ImageProcessor currentImageProcessor) { this.currentImageProcessor = currentImageProcessor; }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public List<Double> getMetricValues() {
        return metricValues;
    }

    public void setMetricValues(List<Double> metricValues) {
        this.metricValues = metricValues;
    }

    public void clearMetricValues() {
        this.metricValues.clear();
    }

    public void addMetricValue(double value) {
        this.metricValues.add(value);
        //System.out.println("added value: "+value);
    }


    public double getStackMaximum() {
        return stackMaximum;
    }

    public void setStackMaximum(double stackMaximum) {
        this.stackMaximum = stackMaximum;
    }

    public ImagePlus getCurrentImageMask() {
        return currentImageMask;
    }

    public void setCurrentImageMask(ImagePlus currentImageMask) {
        this.currentImageMask = currentImageMask;
    }

    public int getThresholdValue() {
        return thresholdValue;
    }

    public void setThresholdValue(int thresholdValue) {
        this.thresholdValue = thresholdValue;
    }

    public boolean isCircleMetric() {
        return circleMetric;
    }

    public void setCircleMetric(boolean circleMetric) {
        this.circleMetric = circleMetric;
    }

    public int getCircleRadius() {
        return circleRadius;
    }

    public void setCircleRadius (int circleRadius) throws Exception {
        // TODO center is not always in the middle
        if (circleRadius> this.height/2 || circleRadius> this.width/2) {
            throw new Exception();
        } else {
            this.circleRadius = circleRadius;
        }
    }

    public double getMetricMaximum() {
        return metricMaximum;
    }

    public void setMetricMaximum(double metricMaximum) {
        this.metricMaximum = metricMaximum;
    }

    public int getWidthMaximum() {
        return widthMaximum;
    }

    public void setWidthMaximum(int widthMaximum) {
        this.widthMaximum = widthMaximum;
    }

    public float getAllowedMaxIntensity() {
        return allowedMaxIntensity;
    }

    public void setAllowedMaxIntensity(float allowedMaxIntensity) {
        this.allowedMaxIntensity = allowedMaxIntensity;
    }
}
