package hu.brc.hpg.imagej;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import javax.swing.UIManager;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.NumberFormatter;

import net.miginfocom.swing.MigLayout;


public class PluginUI {
    // View uses Swing framework to display UI to user
    private JFrame frame;

    /*private JPanel panel1;
    private JPanel panel2;
    private JPanel panel3;*/

    private JLabel metricLabel;
    private JLabel fileListLabel;
    private JLabel statusLabel;
    private JProgressBar progressBar;
    private JLabel messageLabel;
    private JLabel cutoffLabel;
    private JLabel exportLabel;
    private JLabel radiusLabel;
    private JLabel placeholder1;
    private JLabel placeholder2;
    private JLabel placeholder3;
    private JLabel placeholder4;
    private JLabel placeholder5;
    private JLabel imageLabel;
    private JSpinner cutoffSpinner;
    private JSpinner radiusSpinner;
    private JFormattedTextField cutoffField;
    //private JTextField dirField;
    private JComboBox metricList;
    private DefaultListModel listModel;
    private JList fileList;
    private JFileChooser fileChooser;
    private JFileChooser saveFileChooser;
    private JLabel extractChannelLabel;
    private JComboBox channelList;
    private JCheckBox circleCheckbox;
    private JCheckBox thresholdCheckbox;
    private JButton chooseFilesButton;
    private JButton calculateMetricButton;
    private JButton exportDirButton;
    private JButton aboutUsButton;
    private JScrollPane fileScrollPane;
    //private JButton batchButton;
    //private FileNameExtensionFilter filter;

    private String[] choices = {"Intensity variance", "Gradient magnitude variance", "Laplacian variance", "Histogram entropy", "Histogram threshold", "Frequency threshold", "Kurtosis"};
    private String[] channels = {"Channel 1", "Channel 2", "Channel 3"};

    public JButton getAboutUsButton() {
        return aboutUsButton;
    }

    public void setAboutUsButton(JButton aboutUsButton) {
        this.aboutUsButton = aboutUsButton;
    }

    public PluginUI(String title) {

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch (Exception e) {
            System.out.println("Look and feel not set");
        }

        //ImageIcon img = new ImageIcon("D:/Downloads/download.png");

        frame = new JFrame(title);
        //frame.setIconImage(img.getImage());
        frame.getContentPane().setLayout(new BorderLayout());
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setSize(700, 350);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        // Create UI elements
        metricLabel = new JLabel("Metric:");
        cutoffLabel = new JLabel("Pixel cutoff (%):");
        radiusLabel = new JLabel("Radius of circle mask:");
        fileListLabel = new JLabel("Files to compare:");
        exportLabel = new JLabel("Export scores here:");

        statusLabel = new JLabel("Status:");
        progressBar = new JProgressBar();
        progressBar.setMinimum(0);
        progressBar.setMaximum(100);
        progressBar.setStringPainted(true);
        placeholder1 = new JLabel("");
        placeholder2 = new JLabel("");
        placeholder3 = new JLabel("");
        placeholder4 = new JLabel("");
        placeholder5 = new JLabel("");
        messageLabel = new JLabel("No image operations in progress.");
        imageLabel = new JLabel();
        BufferedImage img = null;
        try {

            img = ImageIO.read(getClass().getResource("/aboutus_small.png"));
        } catch (IOException e) {
            System.out.println("not ok");
        }
        ImageIcon icon = new ImageIcon(img);
        imageLabel.setIcon(icon);
        metricList = new JComboBox(choices);

        cutoffSpinner = new JSpinner(new SpinnerNumberModel(0,0,100,0.001));
        radiusSpinner = new JSpinner(new SpinnerNumberModel(200,0,2000,1));
        radiusLabel.setVisible(false);
        radiusSpinner.setVisible(false);

        NumberFormat percentageFormat = NumberFormat.getNumberInstance();
        NumberFormatter percentageFormatter = new NumberFormatter(percentageFormat);
        percentageFormatter.setAllowsInvalid(false);
        percentageFormatter.setMaximum(100.0);
        percentageFormatter.setMinimum(0.0);

        NumberFormat decFormat = DecimalFormat.getInstance();
        decFormat.setMinimumFractionDigits(0);
        decFormat.setMaximumFractionDigits(7);

        // cutoffField = new JFormattedTextField(decFormat);
        cutoffField = new JFormattedTextField(percentageFormat);
        //cutoffField.setFocusLostBehavior(JFormattedTextField.REVERT);
        cutoffField.setText("0");
        listModel = new DefaultListModel();
        fileList = new JList(listModel);
        fileScrollPane = new JScrollPane();
        fileScrollPane.setViewportView(fileList);
        fileChooser = new JFileChooser();
        fileChooser.setMultiSelectionEnabled(true);

        saveFileChooser = new JFileChooser();
        //saveDirChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        //saveDirChooser.setAcceptAllFileFilterUsed(false);
        saveFileChooser.setDialogTitle("Select directory where you want the scores to be saved.");
        saveFileChooser.setSelectedFile(new File("results.csv"));
        //filter = new FileNameExtensionFilter("csv");
        //saveFileChooser.setFileFilter(filter);

        extractChannelLabel = new JLabel("Channel to extract:");
        channelList = new JComboBox(channels);
        circleCheckbox = new JCheckBox("Calculate metric inside a circle");
        thresholdCheckbox = new JCheckBox("Do not threshold");
        chooseFilesButton = new JButton("Choose images to compare...");
        exportDirButton = new JButton("Choose export directory...");
        calculateMetricButton = new JButton("Calculate metric");
        aboutUsButton = new JButton("About us");
        //batchButton = new JButton("Group selected files");


        // Add UI element to frame
        GroupLayout layout = new GroupLayout(frame.getContentPane());
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);


        /*layout.setHorizontalGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(metricLabel)
                                .addComponent(extractChannelLabel)
                                        .addComponent(circleCheckbox)
                                        .addComponent(thresholdCheckbox)
                                .addComponent(chooseFilesButton)
                                .addComponent(fileListLabel)
                                .addComponent(statusLabel)
                                .addComponent(calculateMetricButton))
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(metricList)
                                .addComponent(channelList)
                                .addComponent(placeholder1)
                                .addComponent(placeholder2)
                                .addComponent(placeholder3)
                                .addComponent(fileList)
                                .addComponent(messageLabel)
                                .addComponent(placeholder4)));

        layout.setVerticalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(metricLabel)
                        .addComponent(metricList))//.addComponent(hello))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(extractChannelLabel)
                        .addComponent(channelList))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(circleCheckbox)
                        .addComponent(placeholder1))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(thresholdCheckbox)
                        .addComponent(placeholder2))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(chooseFilesButton)
                        .addComponent(placeholder3))

                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(fileListLabel)
                                .addComponent(fileList))
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(statusLabel)
                                .addComponent(messageLabel))
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(calculateMetricButton)
                                .addComponent(placeholder4)));*/


        layout.setHorizontalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(metricLabel)
                        .addComponent(extractChannelLabel)
                        .addComponent(cutoffLabel)
                        .addComponent(radiusLabel)
                        .addComponent(fileListLabel)
                        .addComponent(exportLabel)
                        .addComponent(statusLabel)
                        .addComponent(aboutUsButton))
                        //.addComponent(placeholder))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(metricList)
                        .addComponent(channelList)
                        .addComponent(cutoffSpinner)
                        .addComponent(radiusSpinner)
                        .addComponent(fileScrollPane)
                        .addComponent(placeholder4)
                        .addComponent(messageLabel)
                        .addComponent(placeholder3))
                        //.addComponent(messageLabel))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(circleCheckbox)
                        .addComponent(thresholdCheckbox)
                        .addComponent(placeholder1)
                        .addComponent(placeholder2)
                        .addComponent(chooseFilesButton)
                        .addComponent(exportDirButton)
                        .addComponent(calculateMetricButton)
                        .addComponent(placeholder5)));
                /*.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(hello)
                        .addComponent(bye)));*/
        layout.setVerticalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(metricLabel)
                        .addComponent(metricList).addComponent(circleCheckbox))//.addComponent(hello))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(extractChannelLabel)
                        .addComponent(channelList).addComponent(thresholdCheckbox))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(cutoffLabel)
                        .addComponent(cutoffSpinner).addComponent(placeholder1))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(radiusLabel)
                        .addComponent(radiusSpinner).addComponent(placeholder2))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(fileListLabel)
                        .addComponent(fileScrollPane).addComponent(chooseFilesButton))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(exportLabel)
                        .addComponent(placeholder4).addComponent(exportDirButton))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(statusLabel)
                        .addComponent(messageLabel).addComponent(calculateMetricButton))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(aboutUsButton)
                        .addComponent(placeholder3).addComponent(placeholder5)));
                /*.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(placeholder)
                        .addComponent(messageLabel)));*/
        layout.linkSize(SwingConstants.HORIZONTAL, chooseFilesButton, exportDirButton, calculateMetricButton);

        //layout.linkSize(SwingConstants.HORIZONTAL, hello, bye);
        frame.getContentPane().setLayout(layout);


    }

    public JButton getExportDirButton() {
        return exportDirButton;
    }

    public JFrame getFrame() {
        return frame;
    }
    public void setFrame(JFrame frame) {
        this.frame = frame;
    }
    public JLabel getMetricLabel() {
        return metricLabel;
    }
    public void setMetricLabel(JLabel metricLabel) {
        this.metricLabel = metricLabel;
    }
    public JLabel getFileListLabel() {
        return fileListLabel;
    }
    public void setFileListLabel(JLabel fileListLabel) {
        this.fileListLabel = fileListLabel;
    }

    public JFileChooser getSaveFileChooser() {
        return saveFileChooser;
    }

    public JComboBox getMetricList() {
        return metricList;
    }

    public int getMetricIndex() {
        return metricList.getSelectedIndex();
    }

    public int getChannelIndex() { return channelList.getSelectedIndex(); }


    public JButton getChooseFilesButton() {
        return chooseFilesButton;
    }
    public void setChooseFilesButton(JButton chooseFilesButton) {
        this.chooseFilesButton = chooseFilesButton;
    }
    public JButton getCalculateMetricButton() {
        return calculateMetricButton;
    }
    public void setCalculateMetricButton(JButton calculateMetricButton) {
        this.calculateMetricButton = calculateMetricButton;
    }

    public JLabel getStatusLabel() {
        return statusLabel;
    }

    public void setStatusLabel(JLabel statusLabel) {
        this.statusLabel = statusLabel;
    }

    public JLabel getMessageLabel() {
        return messageLabel;
    }

    public void setMessage(String msg) {
        this.messageLabel.setText(msg);
        //frame.revalidate();
    }

    public void setProgress(int percentage) {
        this.progressBar.setValue(percentage);
    }

    public JFormattedTextField getCutoffField() { return cutoffField; }

    public double getCutoff() {
        return (Double) cutoffSpinner.getValue(); /*Double.parseDouble(cutoffField.getText());*/
    }

    public int getRadius() {
        return (Integer) radiusSpinner.getValue();
    }

    public JSpinner getCutoffSpinner() {
        return cutoffSpinner;
    }

    public JCheckBox getCircleCheckbox() {
        return circleCheckbox;
    }

    public void setCircleCheckbox(JCheckBox circleCheckbox) {
        this.circleCheckbox = circleCheckbox;
    }

    public JCheckBox getThresholdCheckbox() {
        return thresholdCheckbox;
    }

    public void setThresholdCheckbox(JCheckBox thresholdCheckbox) {
        this.thresholdCheckbox = thresholdCheckbox;
    }

    public JFileChooser getFileChooser() {
        return fileChooser;
    }

    public void showFileChooser() {
        fileChooser.showOpenDialog(frame);
    }

    public void showExportChooser() {
        saveFileChooser.showSaveDialog(frame);
    }

    /*public JButton getBatchButton() {
        return batchButton;
    }

    public void setBatchButton(JButton batchButton) {
        this.batchButton = batchButton;
    }*/

    public JLabel getRadiusLabel() {
        return radiusLabel;
    }

    public JSpinner getRadiusSpinner() {
        return radiusSpinner;
    }

    public void popupMessage(String msg) {
        JTextArea ta = new JTextArea(15,40);
        ta.setFont(new Font("Serif", Font.PLAIN, 16));
        ta.setText(msg);
        ta.setEditable(false);
        JScrollPane areaScrollPane = new JScrollPane(ta);
        areaScrollPane.setVerticalScrollBarPolicy(
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        JOptionPane.showMessageDialog(null, msg, "Notification", JOptionPane.INFORMATION_MESSAGE);
    }

    public void errorMessage(String msg) {
        JTextArea ta = new JTextArea(15,40);
        ta.setFont(new Font("Serif", Font.PLAIN, 16));
        ta.setText(msg);
        ta.setEditable(false);
        JScrollPane areaScrollPane = new JScrollPane(ta);
        areaScrollPane.setVerticalScrollBarPolicy(
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        JOptionPane.showMessageDialog(null, msg, "Error", JOptionPane.ERROR_MESSAGE);
    }

    public void showAboutUs() {
        JFrame frame = new JFrame("About us");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        try
        {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
        panel.setOpaque(true);

        panel.add(imageLabel);


        frame.getContentPane().add(BorderLayout.CENTER, panel);
        frame.pack();
        frame.setVisible(true);
        frame.setResizable(false);
    }

    private boolean isExtensionCorrect(String extension) {
        switch (extension) {
            case "tiff": return true;
            case "tif": return true;
            default: return false;
        }
    }

    public List<File> getChosenFilesList() throws IOException {
        String tmp;
        String extension="";
        File[] files = fileChooser.getSelectedFiles();
        List<File> retList = new ArrayList<File>();
        if (files.length == 0) {
            throw new IOException("No files provided for processing.");
        }
        for (int i=0; i<files.length; i++) {
            tmp = files[i].getName();
            int j = tmp.lastIndexOf('.');
            if (j > 0) {
                extension = tmp.substring(j+1);
            }
            if (!isExtensionCorrect(extension)) {
                throw new IOException("Extension not supported (only tif and tiff at the moment)");
            }
            retList.add(files[i]);
        }
        return retList;
    }

    public void setChosenFilesList() {
        listModel.clear();
        File[] files = fileChooser.getSelectedFiles();
        for (int i = 0; i < files.length; i++) {
            try {
                listModel.add(i, files[i].getCanonicalPath());
            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        }
    }

    public void setSaveFile() {
        File f = saveFileChooser.getSelectedFile();
        placeholder4.setText(f.getAbsolutePath());
    }

    public String getSaveFile() {
        return saveFileChooser.getSelectedFile().getAbsolutePath();
    }

    public void enableAllOptions() {
        circleCheckbox.setEnabled(true);
        thresholdCheckbox.setEnabled(true);
        cutoffSpinner.setEnabled(true);
    }

    public void disableOption(JCheckBox checkBox) {
        checkBox.setSelected(false);
        checkBox.setEnabled(false);
    }
    public void disableOption(JFormattedTextField textField) {
        textField.setText("0");
        textField.setEnabled(false);
    }

    public void disableOption(JSpinner spinner) {
        spinner.setValue(new Double(0));
        spinner.setEnabled(false);
    }

    public void setVisible(JLabel label, boolean value) {
        label.setVisible(value);
    }
    public void setVisible(JSpinner spinner, boolean value) {
        spinner.setVisible(value);
    }


}
